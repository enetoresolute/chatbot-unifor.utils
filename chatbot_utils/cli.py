"""
Execução:

python -m chatbot_utils.cli <option> <operation>
"""
import click
from typing import Text
from chatbot_utils import get_class_with_path
from chatbot_utils.models import *


def execute_cli(option:Text, operation:Text, config:DomainConfig):
    try:                    
        if option == 'redis':
            from chatbot_utils import get_redis_conn

            from rq import Queue
            from redis import Redis
            from chatbot_utils.tests import test_put_solicitation

            if operation == 'start':
                from chatbot_utils.rpa import create_worker
                            
                create_worker(
                    redis_queue=config.REDIS_QUEUE,
                    redis_url=config.REDIS_URL,
                    redis_password=config.REDIS_PASSWORD,
                    sentinel_master_node=config.SENTINEL_MASTER_NODE,
                    sentinel_port=config.SENTINEL_PORT
                )

            if operation == 'list':
                q = Queue(config.REDIS_QUEUE, connection=get_redis_conn())

                print("#   Jobs:",len(q))
                print("Ids Jobs:", q.job_ids)
                print("Det Jobs:", q.jobs)

            if operation == 'delete':
                q = Queue(config.REDIS_QUEUE, connection=get_redis_conn())
                q.delete(delete_jobs=True) # Passing in `True` will remove all jobs in the queue


            if operation == 'add':
                test_put_solicitation()

            if operation == 'listen':
                test_put_solicitation('SOLICITA_CARTEIRINHA_RQ', 1, DomainConfig)
            
            else:
                click.echo("Verifique o tipo. Tipos aceitos: [{}]".format(['list', 'delete']))

        elif option == 'db':
            session = get_session(path=config.DB)

            if operation == 'create':
                create_database(path=config.DB)
        
            elif operation == 'list':
                print(session.query(Solicitation).all())

            elif operation == 'new-solicitation':
                session.add(Solicitation(registration='123456', cia='123456', sender_id='-1'))
                session.commit()

            else:
                click.echo("Verifique o tipo. Tipos aceitos: [{}]".format(['list', 'create']))

    except KeyError as err:
        logger.error(f'Error when evaulating a attribute of config ({config}).\nError: {err}')


@click.command()
@click.argument('option')
@click.argument('operation')
@click.option('-c', '--config', default=None)
def main(option, operation, config):
    click.echo(f"- Option: {option}\n- Operation: {operation}\n- Module Config: {config}")

    if config: 
        # Casos em que o caminho para o arquivo de configuração nos é passado.
        config = get_class_with_path(config)
    
    if config is None:
        logger.warn(f'No one config finded. Assuming DomainConfig as value of config.')
        # Assumindo a configuração padrão 
        config = DomainConfig
    
    execute_cli(option, operation, config)


if __name__ == "__main__":    
    main()