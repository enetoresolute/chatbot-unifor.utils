from os import getenv
from chatbot_utils.models import DomainConfig

class TestConfig(DomainConfig):
    """
    Configuração de teste para a execução do Chatbot
    """
    APP_NAME = DomainConfig.APP_NAME

    DB_ENV = f'{APP_NAME}_DB'
    DB =  getenv(DB_ENV) 

    RASA_SERVER_URL         = getenv(f'{APP_NAME}_RASA_SERVER_URL')
    REDIS_URL               = getenv(f'{APP_NAME}_REDIS_URL')
    REDIS_PASSWORD          = getenv(f'{APP_NAME}_REDIS_PASSWORD')
    REDIS_QUEUE             = getenv(f'{APP_NAME}_REDIS_QUEUE', 'SOLICITA_CARTEIRINHA_RQ')
    SENTINEL_PORT           = getenv(f'{APP_NAME}_SENTINEL_PORT')
    SENTINEL_MASTER_NODE    = getenv(f'{APP_NAME}_SENTINEL_MASTER_NODE')
    SITE_ETUFOR_START       = getenv(f'{APP_NAME}_SITE_ETUFOR_START')
    USERNAME_ETUFOR         = getenv(f'{APP_NAME}_USERNAME_ETUFOR')
    PASSWORD_ETUFOR         = getenv(f'{APP_NAME}_PASSWORD_ETUFOR')
    USER_DBC                = getenv(f'{APP_NAME}_USER_DBC')
    PASSWORD_DBC            = getenv(f'{APP_NAME}_PASSWORD_DBC')
