from chatbot_utils import logger
from chatbot_utils.models import DomainConfig


def test_redis_conn(*args, **kwargs):
    from redis import Redis
    from rq import Queue, Retry
    from rq.registry import FailedJobRegistry

    from chatbot_utils.rpa import request_student_cia
    from chatbot_utils import get_redis_conn

    redis = get_redis_conn(
        redis_url='172.20.101.99',
        **kwargs
        )

    print(redis)


def test_put_solicitation(
    redis_queue,
    solicitation_id,
    config: DomainConfig,
    *args, 
    **kwargs):
    from redis import Redis
    from rq import Queue, Retry
    from rq.registry import FailedJobRegistry

    from chatbot_utils.rpa import request_student_cia
    from chatbot_utils import get_redis_conn
    from chatbot_utils.tests.config import TestConfig

    logger.info("Enfileirando a solicitação: {}".format(solicitation_id))

    redis = get_redis_conn(*args, **kwargs)
    
    q = Queue(redis_queue, connection=redis)
    
    q.enqueue(
        request_student_cia, 
        solicitation_id,
        TestConfig,
        retry=Retry(max=2, interval=[120, 240])
    )


def test_listen_solicitation():
    from chatbot_utils.rpa import create_worker
    from chatbot_utils.models import DomainConfig as config
    
    create_worker(
        redis_queue=config.REDIS_QUEUE,
        redis_url=config.REDIS_URL,
        redis_password=config.REDIS_PASSWORD,
        sentinel_master_node=config.SENTINEL_MASTER_NODE,
        sentinel_port=config.SENTINEL_PORT
    )
