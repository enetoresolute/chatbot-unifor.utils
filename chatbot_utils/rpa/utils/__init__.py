import logging
from datetime import datetime, date
from typing import Text, List

def get_logger(
    level=logging.DEBUG, 
    format="%(levelname)s (%(filename)s:%(lineno)s): (%(asctime)s) %(message)s",
    datefmt="%d-%m %H:%M:%S",
    *args, **kwargs):
    
    logging.basicConfig(level=logging.DEBUG, format="%(levelname)s (%(filename)s:%(lineno)s): (%(asctime)s) %(message)s", datefmt="%d-%m %H:%M:%S")

    return logging.getLogger(__name__)


def parse_datetime(
    datetime_str, 
    formats=['%d/%m/%Y %H:%M', '%Y-%m-%dT%H:%M:%SZ', '%Y-%m-%dT%H:%MZ', '%Y-%m-%dT%H:%M:%S.%fZ', '%Y-%m-%dT%H:%M:%S.%f'], 
    exception=RuntimeError,
    exception_message="Verifique a formatação da data. Ela deve está no formato: ",
    *args,
    **kwargs):
    import dateutil.parser

    logger.debug("Parseando: {}".format(datetime_str))

    for __format in formats:
        try:
            return datetime.strptime(datetime_str, __format)         
        except ValueError as err:
            logger.debug("Erro ao tentar fazer o parse da data: {}".format(str(err)))
            
            try:
                return dateutil.parser.parse(datetime_str).replace(tzinfo=None)
            except Exception as err:
                logger.error("Erro na tentativa de parsear para isoformat: {}".format(err))
            
            #if kwargs.get('make_recursion', True):
            #    return parse_datetime(datetime_str.split('+')[0], make_recursion=False) 

    exception_message = exception_message + ", ".join(formats) + "."
    raise exception(exception_message)


def parse_date( 
    datetime_str: Text, 
    formats=['%d/%m/%Y', '%Y-%m-%d'],
    exception=RuntimeError,
    exception_message="Verifique a formatação da data. Ela deve está no formato: ",
    *args,
    **kwargs) -> datetime:

    if 'T' in datetime_str: datetime_str = datetime_str.split('T')[0]
    
    for __format in formats:
        try:
            return datetime.strptime(datetime_str, __format)         
        except ValueError as err:
            logger.debug("Erro ao tentar fazer o parse da data: {}".format(str(err)))

    exception_message = exception_message + ", ".join(formats) + "."
    
    raise exception(exception_message)


def mount_url(path:Text='credentials.yml', *args, **kwargs) -> Text:
    import yaml

    url_path: Text = None

    with open(path, 'r') as stream:
        try:
            yml = yaml.safe_load(stream)

            if 'telegram' in yml and 'webhook_url' in yml['telegram']:
                url_path = yml['telegram']['webhook_url'].split('/webhooks')[0]

        except yaml.YAMLError as exc:
            logger.exception(exc)

        except Exception as err:
            logger.exception(err)

    return url_path


def holydays(year:int=None, *args, **kwargs) -> List[Text]:
    from workalendar.america import BrazilFortalezaCity

    return [str(tup[0]) for tup in BrazilFortalezaCity().holidays(datetime.now().year)]


logger = get_logger()