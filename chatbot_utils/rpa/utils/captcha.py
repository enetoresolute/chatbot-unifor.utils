from chatbot_utils.death_by_captcha_api import deathbycaptcha
from chatbot_utils.models import DomainConfig as config
from chatbot_utils import logger

from typing import Text
from uuid import uuid4
from pathlib import Path
from urllib.request import urlretrieve
import re
import base64


def decode_base64(data, altchars=b'+/'):
    """Decode base64, padding being optional.

    :param data: Base64 data as an ASCII byte string
    :returns: The decoded byte string.

    """
    data = re.sub(rb'[^a-zA-Z0-9%s]+' % altchars, b'', data)  # normalize
    missing_padding = len(data) % 4
    if missing_padding:
        data += b'='* (4 - missing_padding)
    return base64.b64decode(data, altchars)


def download_by_location(
    webdriver, 
    query_selector:Text='#Table2 > tbody > tr:nth-child(20) > td:nth-child(2) > img', 
    file_name:Text=None) -> Text:

    from io import BytesIO

    if not file_name:
        file_name = '{}.png'.format(str(uuid4()))

    _folder_path = Path(config.TEMP_FILES)

    _file_path = _folder_path / file_name
    _file_path = str(_file_path)

    img_captcha_base64 = webdriver.execute_script("""
        var c = document.createElement('canvas');
        var ctx = c.getContext('2d');
        var img = document.querySelector('{}');
        c.height=img.naturalHeight;
        c.width=img.naturalWidth;
        ctx.drawImage(img, 0, 0,img.naturalWidth, img.naturalHeight);
        var base64String = c.toDataURL();
        return base64String;
    """.format(query_selector))

    img_captcha_base64 = img_captcha_base64.split(',')[1]

    if len(img_captcha_base64) % 4:
        img_captcha_base64 += "=" * (4-len(img_captcha_base64)%4)

    logger.debug("Captcha b64: {}".format(img_captcha_base64))

    with open(_file_path, 'wb') as f:
        f.write(base64.b64decode(img_captcha_base64))

    return _file_path



def download(
    link, 
    file_name=str(uuid4()),
    root_path:Text=config.TEMP_FILES) -> Text:

    _folder_path = Path(root_path)
    _file_path = _folder_path / file_name

    urlretrieve(link, _file_path)   
    
    return str(_file_path)


def download_image():
    pass


def crack_captcha(
    image_file_path:Text,
    user_death_by_captcha:Text,
    password_death_by_captcha:Text) -> Text:
    client = deathbycaptcha.SocketClient(user_death_by_captcha, password_death_by_captcha)

    try:    
        balance = client.get_balance()
        logger.info("DBC Balance: {}".format(balance))

        captcha = client.decode(image_file_path)

        logger.debug("Responsta do Death By Captcha: '{}'".format(captcha))

        if captcha:
            # The CAPTCHA was solved; captcha["captcha"] item holds its
            # numeric ID, and captcha["text"] item its list of "coordinates".
            logger.debug("CAPTCHA {} resolvido!\n- Resolução: {}".format(captcha["captcha"], captcha["text"]))

            if captcha["text"] == '':  # check if the CAPTCHA was incorrectly solved
                client.report(captcha["captcha"])
                logger.error("CAPTCHA {} resolvido sem sucesso!".format(captcha["captcha"]))
                
                raise RuntimeError("CAPTCHA {} resolvido sem sucesso!".format(captcha["captcha"]))

            return captcha['text']

        else:
            raise Exception('Erro ao receber a resposta da DBC. Resposta: {}'.format(captcha))

    except deathbycaptcha.AccessDeniedException:
        raise RuntimeError("Acesso à API DBC negado.")

    except Exception as err:
        raise RuntimeError("Problemas para acessar à DBC API.")
