from typing import Text, Union, Dict, List, Any
from selenium.webdriver import Chrome, Firefox
from selenium.common.exceptions import NoSuchElementException, TimeoutException, \
    StaleElementReferenceException, ElementClickInterceptedException, ElementNotInteractableException
from selenium.webdriver.support.ui import WebDriverWait 
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from random import uniform, randint
from time import sleep
from urllib.request import urlretrieve

from chatbot_utils import logger
from chatbot_utils.models import DomainConfig as config
from pathlib import Path
from uuid import uuid4
from os import getenv


def init(
    chrome: bool = True, 
    proxy:Text = None, 
    fake_user_agent:bool = True, 
    headless:bool = None,
    chrome_path:Text = None,
    *args, 
    **kwargs) -> Union[Chrome, Firefox]:
    
    webdriver = choose_webdriver(chrome, proxy, fake_user_agent, headless, chrome_path, *args, **kwargs)
    webdriver.delete_all_cookies()

    return webdriver


def choose_webdriver(
    chrome: bool            = True, 
    proxy:Text              = None, 
    fake_user_agent:bool    = True, 
    headless:bool           = None,
    chrome_path:Text        = None,
    *args, 
    **kwargs) -> Union[Chrome, Firefox]:
    """
    Função para retornar um webdriver pronto para uso.
    """
    from selenium.webdriver.chrome.options import Options as ChromeOptions
    from selenium.webdriver.firefox.options import Options as FirefoxOptions

    # Definindo as Opções
    if chrome:
        _options = ChromeOptions()
        _options.add_argument('---no-sandbox')
        _options.add_argument("--headless")
        _options.add_argument("--disable-client-side-phishing-detection")
        _options.add_argument("--disable-client-side-phishing-detection")
        _options.add_experimental_option("excludeSwitches", ['enable-automation'])
        _options.headless = True if headless is None else headless
        _options.add_experimental_option('prefs', {'download.prompt_for_download': False, 'plugins.always_open_pdf_externally': True, 'profile.default_content_settings.popups': 0})

        if proxy:
            _options.add_argument('--proxy-server=http://%s' % proxy)

    else:
        _options = FirefoxOptions()


    _options.add_argument('--start-maximized')


    if fake_user_agent:
        from fake_useragent import UserAgent    
        _user_agent = UserAgent()
        _user_agent = _user_agent.random
        _options.add_argument(f'user-agent={_user_agent}')


    if chrome:
        if chrome_path is None:
            chrome_path='chromedriver'
            
        return Chrome(
            executable_path=chrome_path, 
            chrome_options=_options
            )

    return Firefox(executable_path=kwargs['path_firefox_driver'])


def check_exists_by_xpath(
    webdriver:Union[Chrome, Firefox], 
    xpath:Text, 
    returning_element:bool=False, 
    delay:int=10,
    click:bool=False) -> bool:

    wait_until(webdriver,xpath, delay)
    
    try:
        _element = webdriver.find_element_by_xpath(xpath)
    
    except NoSuchElementException:
        return False

    if click:
        click_on_element(webdriver, xpath)

    if returning_element:
        return _element

    return True


def wait_until(
    webdriver:      Union[Chrome, Firefox], 
    xpath:          Text, 
    delay:          int=10, 
    with_exception: bool=True, 
    *args, 
    **kwargs):

    _type =  kwargs.get('tipe_of_wait', 'visibility_of_element_located')
       
    try:
        WebDriverWait(webdriver, delay).until(getattr(EC, _type)((By.XPATH, xpath)) )

    except TimeoutException as err:
        if with_exception:
            raise err
        return False

    except AttributeError:
        raise RuntimeError("Verifique o parâmetro 'tipe_of_wait': {}. Provavelmente, o objeto 'EC' não o possui.".format(_type))


def click_on_element(
    webdriver:Union[Chrome, Firefox],
    xpath:Text,
    with_exception:bool=True,
    *args, 
    **kwargs):
    count =         kwargs.get('count', 0)
    max_count =     kwargs.get('max_count', 5)

    try:
        element = get_by_xpath(webdriver, xpath)
        element.click()
 
        return element    
    
    except (
        NoSuchElementException, 
        ElementClickInterceptedException, 
        StaleElementReferenceException) as err:
        

        """
        Caso tenhamos chegado às tentativas máximas, verificaremos se retornaremos a 
        exceção ou somente None.

        Se não, seguiremos para uma chamada a mais para essa funcionalidade.
        """
        if count == max_count:
            if with_exception:
                raise err
            return None
        
        kwargs['count'] =       count + 1
        kwargs['max_count'] =   max_count

        return click_on_element(
            webdriver, 
            xpath, 
            delay=delay,
            with_exception=with_exception,
            *args,
            **kwargs
            )

    except ElementNotInteractableException as err:
        logger.exception(err)
        return None


def get_by_xpath(
    webdriver           :Union[Chrome, Firefox], 
    xpath               :Text, 
    delay               :int=5,
    with_exception      :bool=True,
    command             :Text='find_element_by_xpath',
    *args,
    **kwargs):
       
    count = kwargs.get('count', 0)

    try:
        wait_until(webdriver, xpath, delay, *args, **kwargs)
        element = getattr(webdriver, command)(xpath)
        return element    
    except (NoSuchElementException, ElementClickInterceptedException, StaleElementReferenceException, TimeoutException) as err:
        if count == kwargs.get('max_count', 5):
            if with_exception:
                raise err
            return None

        else:
            kwargs['count'] = count + 1

            return get_by_xpath(
                    webdriver, 
                    xpath, 
                    delay=delay,
                    with_exception=with_exception,
                    *args,
                    **kwargs
                    )


def type_like_human(
    webdriver: Union[Chrome, Firefox], 
    element, 
    text:Text, 
    speed:tuple = (0.05, 0.1),
    *args, 
    **kwargs):
    """
    Função p/ simular o efeito de digitação de um ser humado
    """
    logger.debug("Digitando como humano: '{}'".format(text))
    
    if kwargs.get('clear', False):
        element.clear()

    if not isinstance(text, str):
        text = str(text)

    for char in text:
        try:
            element.send_keys(char)
            wait_between(*speed)
        except Exception as err:
            logger.error("Erro na tentativa de escrever como humano: {}".format(str(err)))
            raise err
       
    
    wait_between(0.5, 1.5)


def wait_between(a=1, b=1.7):
	rand = uniform(a, b) 
	sleep(rand)


def download_document(link:Text, file_name:Text=str(uuid4()), file_format:Text='pdf') -> Text:
    _folder_path = Path(config.TEMP_FILES)
    _file_path = _folder_path / "{}.{}".format(file_name, file_format)

    urlretrieve(link, _file_path)   
    
    return str(_file_path)


class DomainBot(object):

    

    def __init__(self, 
        config: config, 
        web_driver: Union[Chrome, Firefox] = None, 
        session: Any = None,
        *args, 
        **kwargs):
        
        if not (hasattr(self, 'execute_solicitation') and callable(getattr(self, 'execute_solicitation'))):  
            raise NotImplementedError("Verifique a função 'execute_solicitation'.")
        
        if not (hasattr(self, '_start') and getattr(self, '_start') not in (None, '')):
            raise NotImplementedError("Verifique a função '_start'.")

        self._webdriver:Union[Chrome, Firefox] = web_driver
        self._session = session
        self._config = config

    
    def init_web_driver(self, *args, **kwargs):

        logger.info(f'Init Webdriver with: {self.config.CHROMEDRIVER_PATH}')
        
        self._webdriver = init(
            chrome=True, 
            proxy=None,
            fake_user_agent=False,
            headless=self.config.HEADLESS,
            chrome_path=self.config.CHROMEDRIVER_PATH
            )


    @property
    def config(self) -> config:
        return self._config

    
    @property
    def webdriver(self) -> Union[Chrome, Firefox]:
        if not self._webdriver:
            self.init_web_driver()

        return self._webdriver


    @property
    def start(self) -> Text:
        if not self._start:
            raise NotImplementedError()

        return self._start


    @property
    def session(self):
        if not self._session:
            from chatbot_utils.models import get_session
            self._session = get_session(path=self._config.DB)

        return self._session
        

    @property
    def rasa_append_message(self) -> Text:
        """
        Propriedade para retornar o endereço para adição de uma mensagem à um usuário.
        """
        return f'{self.config.RASA_SERVER_URL}/webhooks/socketio/append'


    @property
    def rasa_link_message(self) -> Text:
        """
        Propriedade para retornar o endereço para um link de uma mensagem de anexo.
        """
        return f'{self.config.RASA_SERVER_URL}/webhooks/socketio/file'


    def close_webdriver(self, *args, **kwargs):
        try:
            self._webdriver.close()
            self._webdriver.quit()
        except AttributeError as err:
            logger.error('Error when close the driver: {}'.format(err))
        