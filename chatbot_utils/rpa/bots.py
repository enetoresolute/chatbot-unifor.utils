from chatbot_utils.rpa.utils.webdriver import (
    DomainBot, 
    get_by_xpath, 
    wait_between, 
    type_like_human, 
    click_on_element, 
    wait_until, 
    NoSuchElementException, 
    download_document, 
    check_exists_by_xpath)

from typing import Text, Union, Dict, List
from selenium.webdriver import Chrome, Firefox
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait 
from selenium.common.exceptions import (
    NoSuchElementException, 
    TimeoutException,
    StaleElementReferenceException, 
    ElementClickInterceptedException, 
    ElementNotInteractableException,
    UnexpectedAlertPresentException as AlertException)

from chatbot_utils import logger, parse_date
from chatbot_utils.models import Solicitation, ModelStatus, Attachment, DomainConfig
from chatbot_utils.exceptions import CiaNotFoundException, FlowException, StopSolicitationException, AlreadyExistsFirstPaymentException, \
    ErrorWhileGetPdfException
from chatbot_utils.rpa.utils.captcha import download, crack_captcha, download_by_location
from uuid import uuid1
from datetime import datetime


class SolicitaCarteirinhaBot(DomainBot):
    __db = [{'cia': '0143345001', 'solicitation_id': 1} ]    
    _start = DomainConfig.SITE_ETUFOR_START
    
    """
    Usuário de acesso login da Etufor
    """
    _username = DomainConfig.USERNAME_ETUFOR 
    _password = DomainConfig.PASSWORD_ETUFOR 

    """
    Usuário de acesso ao DBC
    """
    _user_dbc = DomainConfig.USER_DBC
    _password_dbc = DomainConfig.PASSWORD_DBC


    def execute_solicitation(self, solicitation: Solicitation, *args, **kwargs):
        """
        1º: Fazendo o login na plataforma antes de utilizá-la
        """
        self.webdriver.get(self._start)
        self.process_login()       
        self.process_solicitation()

        try:
            self.process_create_carteirinha(solicitation.user_informations)
            
            solicitation.status = ModelStatus.CONCLUDED.value
            solicitation.close()

            # Carregando o Attachment montado e capturado
            attachment = self.session.query(Attachment).filter_by(solicitation_id=solicitation.id).order_by(desc(Attachment.created_at)).first()
            
            if not attachment:
                raise RuntimeError(f'Não foi possível encontrar o Anexo após o fim da solicitação: {solicitation.id}')

            link_url = f'{self.rasa_link_message}/{attachment.id}'

            solicitation.send_process_carteirinha(
                url=self.rasa_append_message,
                link_url=link_url,
            )

        except (AlreadyExistsFirstPaymentException, ) as err:
            solicitation.status = ModelStatus.CONCLUDED_WITH_EXCEPTION.value
            solicitation.close()  
            
            solicitation.send_already_has_first_version(
                url=self.rasa_append_message
            )

            raise StopSolicitationException(solicitation.id, description='Solicitação encerrada! O boleto já foi emitido.')
        
        except (ErrorWhileGetPdfException, ) as err:
            solicitation.status = ModelStatus.CONCLUDED_WITH_EXCEPTION.value
            solicitation.close()  

            message = '🤔 Pelo que verifiquei aqui, o portal da Etufor encontrou um probleminha na sua solicitação. Quando isso ocorre, posso aconselhar:\n\n\n'
            message += '1️⃣ Entrar em contato com a equipe da Unifor\n\n'
            message += '2️⃣ Procurar diretamente a Etufor com seu documento de identidade e solicitar ajuda por lá mesmo'

            solicitation.send(message, url=self.rasa_append_message)            
            
            raise StopSolicitationException(solicitation.id, description='Solicitação encerrada! Erro na hora de receber o .pdf')

        except Exception as err:
            """
            Erros de fluxo comuns quando algum elemento do selenium não consegue ser acessado
            """
            if solicitation.attempt == 3:
                solicitation.status = ModelStatus.ERROR_PROCESSING.value
                solicitation.close()            
              
                raise StopSolicitationException(solicitation.id, description='Solicitação encerrada! Quantidade de tentativas estouradas!')
            
            raise err

        finally:
            self.session.commit()
            self.session.close()       
        


    def process_create_carteirinha(
        self,
        user_informations:dict,
        iframe_xpath:Text                   = '/html/frameset/frame[2]',
        cia_xpath:Text                      ='//*[@id="txtCIA"]',
        complete_name_xpath:Text            ='//*[@id="txtNomeAlunoConsulta"]',
        mother_name_xpath:Text              ='//*[@id="txtNomeMaeConsulta"]',
        birth_date_xpath:Text               ='//*[@id="txtDataNascimentoConsulta"]',
        consult_button_xpath:Text           ='//*[@id="btnPesquisar"]',
        open_information_button_xpath:Text  ='//*[@id="grdAlunos__ctl2_Imagebutton1"]',
        user_informations_table_xpath:Text  ='//*[@id="pnlDados"]',
        *args,
        **kwargs):

        # Verificando se o captcha está com seu iframe aberto:
        iframeSwitch = self.webdriver.find_element_by_xpath(iframe_xpath)
        
        self.webdriver.switch_to.frame(iframeSwitch)
        
        wait_between(2, 4)
        
        """
        Inserindo as informações do usuário na entrada inicial.
        """
        if 'cia' in user_informations:
            type_like_human(
                self.webdriver, 
                get_by_xpath(self.webdriver, cia_xpath),
                user_informations['cia'],
                clear=True
                )
        
        else:
            if not ('name' in user_informations and 'mother_name' in user_informations and 'birth_date' in user_informations):
                raise RuntimeError("Verifique a entrada de variáveis do user_informations.")


            parsed_date = parse_date(user_informations['birth_date']).strftime('%d/%m/%Y')
                    
            type_like_human(
                self.webdriver, 
                get_by_xpath(self.webdriver, complete_name_xpath),
                user_informations['name'],
                speed=(0.01, 0.08),
                clear=True
                )
            
            type_like_human(
                self.webdriver, 
                get_by_xpath(self.webdriver, mother_name_xpath),
                user_informations['mother_name'],
                speed=(0.1, 0.2),
                clear=True
                )

            type_like_human(
                self.webdriver, 
                get_by_xpath(self.webdriver, birth_date_xpath),
                parsed_date,
                speed=(0.1, 0.2),
                clear=True
                )
        
        """
        Clicando no botão 'consultar'. Após, clicando no botão '+ Abrir' caso o usuário seja encontrado

        Um alerta pode ser emitido em casos onde a informação não se apresenta correta
        """
        try:
            click_on_element(
                self.webdriver,
                consult_button_xpath
            )
        except AlertException as err:
            raise StopSolicitationException(solicitation_id=solic)
        

        wait_until(
            self.webdriver,
            open_information_button_xpath
            )

        wait_between(1, 2)

        click_on_element(
            self.webdriver,
            open_information_button_xpath
            )


        """
        Se for encontrado algum dado do usuário.

        Este usuário pode possuir campos faltantes que será necessário a entrada de novos dados. Para isso, vamos varrer
        a 'Table2', cada um dos seus Trs

        Pode ser necessários os adicionar em um atributo e salvá-lo para podermos retornar para o Usuário.

        Pode ser necessário atualizar:
        
        - //*[@id="txtAtualizarTelefone"]
        """
        informations_table = get_by_xpath(self.webdriver, '//*[@id="Table2"]')

        table_rows = informations_table.find_elements_by_tag_name('tr')

        data = []

        valid_text = lambda text: isinstance(text, str) and text != None and text.strip() != ''

        """
        O estado da interação pode ser: CURRENT_DATA (dados atuais cadastrados), TO_UPDATE (dados faltantes no cadastro) e CAPTCHA (entrada para o captcha)
        """
        STATE: Text = 'CURRENT_DATA'
        last_td_text: Text = None

        for row in table_rows:
            data.append([])

            """
            1º: Acessando todos os filhos, td da tag tr
            """
            for td in row.find_elements_by_tag_name('td'):
                
                """
                2°: Acessando e armazenando o texto extraído do td
                """
                text = td.text

                logger.debug("Texto corrente: {}".format(text))
            
                """
                Mudando estado do robô
                """
                if text == 'Atualize os dados abaixo:':
                    STATE = 'TO_UPDATE'
                    logger.debug("Mudando estado para TO_UPDATE")
                    continue

                if text == 'Digite abaixo os números da figura:':
                    STATE = 'CAPTCHA'
                    logger.debug("Mudando estado para CAPTCHA")
                    continue

                if STATE == 'CURRENT_DATA':
                    pass
                
                """
                Avaliando entrada para um dado estado
                """
                if STATE == 'TO_UPDATE':
                    """
                    Selecionando o primeiro filho do td. Ele pode ser um span, input ou nada.
                    """
                    try:
                        first_child = td.find_element_by_css_selector("*")

                    except NoSuchElementException as err:
                        logger.warning("Nenhum filho encontrado: {}".format(err))
                        continue

                    tag = first_child.tag_name

                    if tag == 'input':
                        type_like_human(
                            self.webdriver,
                            first_child,
                            self.__get_text_to_input(last_td_text, user_informations) # Passando o último texto do td para sabermos o que o input necessita
                        )

                if STATE == 'CAPTCHA':
                    pass

                # Armazenando o último elemento de texto
                last_td_text = text
                
                if valid_text(text): 
                    data[-1].append(text)

        logger.debug("Dados raspados: {}".format(data))
        
        """
        Verificando se há captcha para ser quebrado. Se sim, devemos enviálo para o DBC.
        """
        if check_exists_by_xpath(self.webdriver, '//*[@id="Table2"]/tbody/tr[20]/td[2]/img'):
            element = get_by_xpath(self.webdriver, '//*[@id="Table2"]/tbody/tr[20]/td[2]/img')

            file_path = download_by_location(self.webdriver)

            logger.info("Endereço do Captcha baixado: {}".format(file_path))

            captcha_value = crack_captcha(file_path, self._user_dbc, self._password_dbc)

            logger.info("Valor processado do captcha: {}".format(captcha_value))

            """
            Preenchendo o valor do captcha no campo de input
            """
            type_like_human(
                self.webdriver, 
                get_by_xpath(self.webdriver, '//*[@id="txtCaptcha"]'),
                captcha_value,
                clear=True
                )
            
            click_on_element(self.webdriver, '//*[@id="btnSolicitar"]')

            try:
                """
                Fechando algum alerta caso este seja aberto
                """
                WebDriverWait(self.webdriver, 10).until(EC.alert_is_present())
               
                alert = self.webdriver.switch_to.alert

                text_of_alert:Text = alert.text

                if text_of_alert:
                    from unidecode import unidecode
                    """
                    Caso 1º: Saída do alert é 'Já possui 1ª Via de Carteira (Paga). Para solicitar uma Nova Via procure a ETUFOR'
                    """
                    __text_of_alert = unidecode(text_of_alert).lower()
                    
                    alert.accept()

                    if 'ja possui 1a via de carteira' in __text_of_alert:
                        # Fazer envio desse status para o usuário:
                        raise AlreadyExistsFirstPaymentException(user_informations['solicitation_id'], text_of_alert)

                alert.accept()
            
            except TimeoutException as err:
                logger.exception("Nenhum alerta encontrado: {}".format(err))

                # Continuando o fluxo de conversa
        
        self.webdriver.switch_to.default_content()  
   
        # Aguardando a tela de boletos ser montada
        logger.debug("Aguardando a tela de boletos ser montada...")
        wait_until(self.webdriver, '/html/frameset')

        self.webdriver.switch_to.frame(get_by_xpath(self.webdriver, '/html/frameset/frame[2]'))

        wait_between(3, 5)

        """
        attachment = Attachment(
            name='temp/f81400ef-a0af-47fd-a15f-9b272e5db0c0.pdf', 
            path='temp/f81400ef-a0af-47fd-a15f-9b272e5db0c0.pdf', 
            solicitation_id=user_informations['solicitation_id'], 
            created_at=datetime.now())
        
        self.session.add(attachment)
        self.session.commit()

        return 
        """

        document = get_by_xpath(self.webdriver, '//*[@id="main-content"]/a', with_exception=False)
        logger.debug('[DEBUG] inicio de log')
        logger.debug('[DEBUG] pegando xpath //*[@id="main-content"]/a: ', document)
        logger.debug('[DEBUG] fim do log')


        if not document:
            """
            A região de click pode não está disponível por algum erro em relação a Etufor. Verificando qual o erro e entregando este para
            o usuário
            """
            logger.debug('A região de click pode não está disponível.')

            body = get_by_xpath(self.webdriver, '/html/body', with_exception=False)

            error_text = body.text if body else 'Algum erro ocorreu no Portal da Etufor na hora de solicitar o boleto. Entre em contato com a etufor'
            
            raise ErrorWhileGetPdfException(user_informations['solicitation_id'], error_text)
            
        document = document.get_attribute('href')
        print('[DEBUG] inicio de log')
        print('[DEBUG] pegando attr href: ', document)
        print('[DEBUG] fim do log')

        logger.debug("Documento p/ download: {}".format(document))

        document = download_document(document)
        
        logger.debug("Local de download: {}".format(document))

        attachment = Attachment(
            name=document, 
            path=document, 
            solicitation_id=user_informations['solicitation_id'], 
            created_at=datetime.now())

        self.session.add(attachment)

        self.session.commit()

        logger.info("Processo finalizado!")


    def process_login(
        self,
        cec_xpath:Text          = '/html/body/table/tbody/tr[3]/td/input',
        password_xpath:Text     = '/html/body/table/tbody/tr[5]/td/input',
        enter_xpath:Text        = '/html/body/table/tbody/tr[6]/td/input'):

        type_like_human(
            self.webdriver, 
            get_by_xpath(self.webdriver, cec_xpath), 
            self._username
            )
        
        type_like_human(
            self.webdriver, 
            get_by_xpath(self.webdriver, password_xpath), 
            self._password
            )

        click_on_element(self.webdriver, enter_xpath)

        wait_between(5, 7)


    def process_solicitation(
        self,
        iframe_xpath:Text = '/html/frameset/frame[2]',
        button_xpath:Text = '//*[@id="btnSolicitarCarteira"]'):

        self.webdriver.switch_to.window(self.webdriver.window_handles[0])
        
        wait_between(2, 4)

        # Verificando se o captcha está com seu iframe aberto:
        iframeSwitch = self.webdriver.find_element_by_xpath(iframe_xpath)
        
        self.webdriver.switch_to.frame(iframeSwitch)
        
        wait_between(2, 4)

        logger.debug("Quantidade de abas: {}".format(self.webdriver.window_handles))
        
        click_on_element(self.webdriver, button_xpath)

        wait_between(5, 7)
        
        self.webdriver.switch_to.default_content()  


    def __get_text_to_input(self, required_input: Text, user_informations:dict, *args, **kwargs) -> Text:
        required_input = required_input.lower()

        if 'telefone' in required_input:
            return user_informations['phone']

        if 'email' in required_input or 'e-mail' in required_input:
            return user_informations['email']


    def __has_update(self, data: List, *args, **kwargs):
        for index, array in enumerate(data):
            for element in array:
                if element == 'Atualize os dados abaixo:':
                    return index, True

        return None, False


    def __filter_elements_to_complete_form(self, data:List, *args, **kwargs) -> List:
        from re import sub

        logger.debug("Filtrando dados faltantes: {}".format(data))

        filtered = []

        for array in data:
            if not len(array): continue

            for element in array:
                if isinstance(element, str):
                    if element == 'Atualize os dados abaixo:':              continue
                    if element == 'Digite abaixo os números da figura:':    continue
                    
                    only_valid_chars_str = sub(r'[^a-zA-Z0-9]', '', element)

                    if only_valid_chars_str != '': filtered.append(only_valid_chars_str)


        return list(set(filtered))


class VerificarCiaBot(DomainBot):
    __db = [{'name': 'Eduardo Pereira da Silva Neto', 'birth_date': '25/04/1993'}]
    _start = 'http://www.etufor.ce.gov.br/consultasolicitacao.asp'


    def execute_solicitation(self, solicitation: Solicitation, *args, **kwargs) -> bool:
        """
        Função para executar uma solicitação e verificar se um dado usuário possui CIA.
        """
        try:
            self.webdriver.get(self.start)
            self.process_verify_cia(solicitation)
            
        
        except CiaNotFoundException as err:
            """
            Cia não encontrado. Encaminhar solicitação para a Etufor.
            """
            logger.error("Cia não encontrado: {}".format(err))

            solicitation.status = ModelStatus.CIA_NOT_FOUND.value
            solicitation.close()

            solicitation.send_cia_not_found(
                url=self.rasa_append_message
            )

            self.session.commit()
            self.session.close()
            
            logger.info("Encerrando caso de CIA não encontrado!")

            raise err

        except Exception as err:
            """
            Erro de timeout - clássico para conexões recusadas
            """
            logger.error("Exception: {}".format(err))

            logger.info("Quantidade de tentativas: {}".format(solicitation.attempt))
            
            if solicitation.attempt == 3:
                solicitation.status = ModelStatus.ERROR_PROCESSING.value
                solicitation.close()            
                
                self.session.commit()
                self.session.close()
                
                raise StopSolicitationException(solicitation.id, description='Solicitação encerrada! Quantidade de tentativas estouradas!')
           
            raise err
       
        """
        Mudando o status para CIA encontrado
        """
        solicitation.status = ModelStatus.CIA_FOUND.value

        self.session.commit()


    def process_verify_cia(
        self,
        solicitation:Solicitation,
        name_xpath:Text                     = '/html/body/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]/td/input',
        day_xpath:Text                      = '/html/body/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[5]/td/input[1]',
        month_xpath:Text                    = '/html/body/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[5]/td/input[2]',
        year_xpath:Text                     = '/html/body/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[5]/td/input[3]',
        cia_xpath:Text                      = '/html/body/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[8]/td/input',
        consult_button_xpath:Text           = '/html/body/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[8]/td/div/input[1]',
        *args,
        **kwargs):

        logger.info("Iniciando: {} - {}".format(self.webdriver, solicitation.user_informations))

        user_informations = solicitation.user_informations

        """
        Inserindo as informações do usuário na entrada inicial.
        """
        if 'cia' in user_informations:
            type_like_human(
                self.webdriver, 
                get_by_xpath(self.webdriver, cia_xpath),
                user_informations['cia'],
                clear=True
                )
        
        else:
            if not ('name' in user_informations and 'birth_date' in user_informations):
                raise RuntimeError("Verifique a entrada de variáveis do user_informations: {}".format(user_informations))

            parsed_date = parse_date(user_informations['birth_date'])  

            type_like_human(
                self.webdriver, 
                get_by_xpath(self.webdriver, name_xpath),
                user_informations['name'],
                speed=(0.01, 0.08),
                clear=True
                )
            
            type_like_human(
                self.webdriver, 
                get_by_xpath(self.webdriver, day_xpath),
                parsed_date.day,
                speed=(0.1, 0.2),
                clear=True
                )

            type_like_human(
                self.webdriver, 
                get_by_xpath(self.webdriver, month_xpath),
                parsed_date.month,
                speed=(0.1, 0.2),
                clear=True
                )
            
            type_like_human(
                self.webdriver, 
                get_by_xpath(self.webdriver, year_xpath),
                parsed_date.year,
                speed=(0.1, 0.2),
                clear=True
                )

        """
        Clicando no botão 'consultar'. Uma aba será aberta com as informações
        """   
        len_of_tabs = len(self.webdriver.window_handles)

        click_on_element(
            self.webdriver,
            consult_button_xpath
            )

        wait_between(3, 5)
        
        if len_of_tabs == len(self.webdriver.window_handles):
            raise FlowException(solicitation.id, "Nenhuma aba aberta. Erro de fluxo.")
        
        # Indo para a última tab
        self.webdriver.switch_to.window(self.webdriver.window_handles[-1])

        wait_between(3, 5)

        table = self.webdriver.find_element_by_xpath('//*[@id="grdAluno"]')

        trs = table.find_elements_by_tag_name('tr')

        logger.info("Quantidade de elementos encontrados: {}".format(len(trs) - 1))

        if len(trs) <= 1:
            raise CiaNotFoundException(solicitation.id, "Nenhum CIA encontrado.")
        
        logger.info("Solicitação com CIA encontrado!")

        #solicitation.send('Encontrei seu Cia. Vou finalizar o processo de solicitação.')



