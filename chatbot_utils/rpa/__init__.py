from rq import Worker
from typing import Text, Union, Any
from chatbot_utils.models import DomainConfig


def create_worker(
    redis_queue:Text,
    redis_url:Text,
    redis_password:Text=None,
    sentinel_master_node:Text=None,
    sentinel_port:Text=None,
    *args, 
    **kwargs) -> Worker:
    """
    Função para criar um worker padrão
    """
    import sys
    from rq import Connection, Queue
    from os import getenv
    from chatbot_utils import logger, get_redis_conn

    redis_conn = get_redis_conn(
        redis_url=redis_url,
        redis_password=redis_password,
        sentinel_master_node=sentinel_master_node,
        sentinel_port=sentinel_port
    )

    with Connection(connection=redis_conn):
        w = Worker(redis_queue)
        w.work()
    

def request_student_cia(
    solicitation_id, 
    config:DomainConfig,
    *args,
    **kwargs):

    from chatbot_utils.rpa.bots import DomainBot, VerificarCiaBot, SolicitaCarteirinhaBot
    from chatbot_utils.models import Solicitation, get_session, close_session, ModelStatus
    from chatbot_utils.exceptions import StopSolicitationException, CiaNotFoundException
    from chatbot_utils import logger

    bot: Union[VerificarCiaBot, SolicitaCarteirinhaBot] = None

    session = get_session(path=config.DB)
    
    solicitation: Solicitation = session.query(Solicitation).filter_by(id=solicitation_id).first()

    if not solicitation:
        logger.error("Solicitação ({}) não encontrada!".format(solicitation_id))
        return True
    
    solicitation.attempt += 1

    """
    Casos em que a solicitação possui um número menor do que o o status CIA_FOUND (2).

    Ou seja:  
    
    ERROR_PROCESSING    = -1   
    PROCESSING          = 0
    CIA_NOT_FOUND       = 1
    """
    if solicitation.status < ModelStatus.CIA_FOUND.value:

        try:
            bot = VerificarCiaBot(
                config=config,
                session=session
                )

            bot.execute_solicitation(solicitation)
                        
        except (CiaNotFoundException, StopSolicitationException):
            """
            Casos em que podemos encerrar a solicitação com o CIA não encontrado e retornando a mensagem para o usuário.

            Em casos em que essa exceção ocorrer, a sessão será encerrada, assim como o webdriver e o processo poderá ser retornado.
            """
            return True
        
        except Exception as err:
            """
            Casos em que algum erro nos foi lançado e temos que tratá-lo caso seja a terceira tentativa.
            """
            raise err
            
        finally:
            bot.close_webdriver()
  
    """
    Se for encontrado um CIA para o aluno, podemos seguir o fluxo para a emissão convencional
    """
    try:
        bot = SolicitaCarteirinhaBot(
            config=config,
            session=session
            )

        bot.execute_solicitation(solicitation)

    except StopSolicitationException:
        """
        Encerrando o processamento do robô em casos que há o estouro de tentativas
        """
        return True
    
    except Exception as err:
        """
        Casos em que algum erro nos foi lançado e ainda não tivemos todas as tentativas concluídas
        """
        raise err
    
    finally:
        bot.close_webdriver()
    
