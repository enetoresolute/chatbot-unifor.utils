from chatbot_utils import logger

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime, Float, TIMESTAMP, JSON, create_engine, ForeignKey, desc
from sqlalchemy import Text as SqlAlchemyText
from sqlalchemy.orm import sessionmaker, scoped_session

from datetime import datetime
from os import getenv
from typing import Text, List, Dict, Union
import enum

Base = declarative_base()
Engine = None



class DomainConfig(object):
    APP_NAME = "CHATBOT"
    APP_ENV = getenv(f'{APP_NAME}_ENV', 'DEV')

    TEMP_FILES = getenv(f'{APP_NAME}_TEMP', '/app/temp')

    DB                      = getenv(f'{APP_NAME}_DB')
    CHROMEDRIVER_PATH       = getenv(f'{APP_NAME}_CHROMEDRIVER_PATH') 
    HEADLESS                = getenv(f'{APP_NAME}_HEADLESS') 

    RASA_SERVER_URL         = getenv(f'{APP_NAME}_RASA_SERVER_URL')
    REDIS_URL               = getenv(f'{APP_NAME}_REDIS_URL')
    REDIS_PASSWORD          = getenv(f'{APP_NAME}_REDIS_PASSWORD')
    REDIS_QUEUE             = getenv(f'{APP_NAME}_REDIS_QUEUE', 'SOLICITA_CARTEIRINHA_RQ')
    SENTINEL_PORT           = getenv(f'{APP_NAME}_SENTINEL_PORT')
    SENTINEL_MASTER_NODE    = getenv(f'{APP_NAME}_SENTINEL_MASTER_NODE')
    SITE_ETUFOR_START       = getenv(f'{APP_NAME}_SITE_ETUFOR_START')
    USERNAME_ETUFOR         = getenv(f'{APP_NAME}_USERNAME_ETUFOR')
    PASSWORD_ETUFOR         = getenv(f'{APP_NAME}_PASSWORD_ETUFOR')
    USER_DBC                = getenv(f'{APP_NAME}_USER_DBC')
    PASSWORD_DBC            = getenv(f'{APP_NAME}_PASSWORD_DBC')


class Solicitation(Base):
    __tablename__ = 'solicitations'
    
    id              = Column('id', Integer, primary_key=True)
    registration    = Column('registration', String(255), nullable=False)
    cia             = Column('cia', String(255), nullable=True)
    name            = Column('name', String(255), nullable=True)
    mother_name     = Column('mother_name', String(255), nullable=True)
    phone           = Column('phone', String(255), nullable=True)
    birth_date      = Column('birth_date', String(255), nullable=True)
    email           = Column('email', String(255), nullable=True)
    status          = Column('status', Integer(), nullable=False, default=1)
    sender_id       = Column('sender_id', String(255), nullable=False)
    channel         = Column('channel', String(255), nullable=True)
    attempt         = Column('attempt', Integer(), nullable=False, default=0)
    created_at      = Column('created_at', DateTime(), nullable=True, default=lambda _: datetime.now())
    closed_at       = Column('closed_at', DateTime(), nullable=True)


    def put_on_queue(self,
        config:DomainConfig,
        *args, 
        **kwargs):
        """
        Função para adicionar uma solicitação à fila.
        """
        from redis import Redis
        from rq import Queue, Retry
        from rq.registry import FailedJobRegistry

        from chatbot_utils.rpa import request_student_cia
        from chatbot_utils import get_redis_conn

        logger.info("Enfileirando a solicitação: {}".format(self.id))

        redis = get_redis_conn(
            redis_url=config.REDIS_URL,
            redis_password=config.REDIS_PASSWORD,
            sentinel_master_node=config.SENTINEL_MASTER_NODE,
            sentinel_port=config.SENTINEL_PORT,
            *args, 
            **kwargs)
        
        q = Queue(
            config.REDIS_QUEUE, 
            connection=redis
            )
        
        q.enqueue(
            request_student_cia, 
            self.id,
            config,
            retry=Retry(max=2, interval=[120, 240])
        )


    def send(self, 
        message:Union[Text, List[Text]], 
        url:Text, 
        *args, 
        **kwargs):
        """
        Função para fazer o devido despacho de uma ou várias mensagens para o servidor do Rasa. 
        """
        import requests
        
        if isinstance(message, Text):
            message = [message]

        for m in message:
            payload = {'sender_id' : self.sender_id, 'text': m}
            payload = {**payload, **kwargs}
            requests.post(url, json=payload)

        return True        
       

    def send_cia_not_found(self,
        url:Text, 
        *args, 
        **kwargs) -> bool:
        """
        Função para fazer o envio para o canal adequado, informando que não foi encontrado o cia do usuário.
        """        
        message = '🤔 O seu número de CIA informado não foi encontrado! Nesses casos, posso te aconselhar duas coisas:\n\n\n'
        message += '1️⃣ Revisar seu número de CIA e refazer a sua solicitação aqui comigo\n\n'
        message += '2️⃣ Procurar diretamente a Etufor com seu documento de identidade e solicitar ajuda por lá mesmo'

        self.send(message, url)


    def send_process_carteirinha(self, 
        url:Text, 
        link_url: Text,
        *args, 
        **kwargs) -> bool:
        """
        Função para fazer o envio para o canal adequado, informando que a carteirinha já foi processada, retornando o boleto para esse usuário.

        :url: endereço base do servidor que recepcionará a mensagem

        :link_url: endereço para download do documento em questão
        """
        message = '✔️ Terminei a sua solicitação! Vou te entregar aqui o [boleto]({}) 💴 para o devido pagamento! \n\n'.format(link_url)
        message += '⌛⌛ Depois, após o pagamento, é só aguardar o retorno do DCE para buscar a sua carteirinha \n\n'

        self.send(message, url, parse_mode='Markdown')


    def send_already_has_first_version(self, 
        url:Text, 
        *args, 
        **kwargs) -> bool:
        message = '🤔 Pelo que verifiquei aqui, você já pagou a 1º via da sua carteirinha. Nesses casos posso aconselhar:\n\n\n'
        message += '1️⃣ Aguardar mais um pouquinho o retorno do DCE\n\n'
        message += '2️⃣ Procurar diretamente a Etufor com seu documento de identidade e solicitar ajuda por lá mesmo'

        self.send(message, url)


    def get_formatted_id(self, *args, **kwargs) -> Text:
        return "{:06d}".format(self.id)


    def get_etufor_link(self, *args, **kwargs) -> Text:
        return 'http://www.etufor.ce.gov.br/consultasolicitacao.asp'


    def get_message_will_init_process(self, *args, **kwargs) -> Text:
        """
        Método que retorna a mensagem de início do processamento
        """        
        message =  "⌛ Submeti a busca! Em breve estarei te retornando o resultado!"
        message += "📝 O número da sua solicitação é: {}.\n\n".format(self.get_formatted_id())
        message += "😁 Qualquer dúvida sobre a sua solicitação, pode me chamar a qualquer momento..\n\n"
        message += "👋🏻 Até mais!"

        return message


    def close(self, *args, **kwargs):
        self.closed_at = datetime.now()


    def is_in_business_interval(self, *args, **kwargs) -> bool:
        from numpy import busday_count

        if not self.closed_at:
            raise RuntimeError("Verifique o atributo 'close_at'")
       
        return busday_count(self.closed_at.date(), datetime.now().date()) <= 2

    def get_attachment_link(self, config=DomainConfig, session=None, *args, **kwargs) -> Text:
        # Carregando o Attachment montado e capturado
        if session is None:
            session = get_session()

        attachment = session.query(Attachment).filter_by(solicitation_id=self.id).order_by(desc(Attachment.created_at)).first()
        
        if not attachment:
            raise RuntimeError("Não foi possível encontrar o Anexo após o fim da solicitação: {}".format(self.get_formatted_id()))
        
        link_url = f"{config.RASA_SERVER_URL}/webhooks/socketio/file/{attachment.id}"
        # Retornando link formatado
        return link_url

    @property
    def user_informations(self) -> dict:
        """
        Formatação de informações de usuário para Formulários do sistema da Etufor.
        """
        if self.cia:
            return {'cia': self.cia, 'registration': self.registration, 'solicitation_id': self.id}
        
        return {
            'name': self.name,
            'mother_name': self.mother_name,
            'birth_date': self.birth_date,
            'phone': self.phone,
            'email': self.email,
            'registration': self.registration,
            'solicitation_id': self.id
        }


class Attachment(Base):
    __tablename__ = 'attachments'
    
    id              = Column('id', Integer, primary_key=True)
    name            = Column('name', String(255), nullable=False)
    path            = Column('path', SqlAlchemyText(), nullable=False)
    solicitation_id = Column('solicitation_id', Integer(), ForeignKey('solicitations.id'), nullable=False)
    created_at      = Column('created_at', DateTime(), nullable=True, default=lambda _: datetime.now())
    status          = Column('status', Integer(), nullable=False, default=1)


class Message(Base):
    __tablename__ = 'messages'
    
    id              = Column('id', Integer, primary_key=True)
    sender_id       = Column('sender_id', String(255), nullable=False)
    type            = Column('type', String(30), nullable=False)
    _body           = Column('body', SqlAlchemyText(), nullable=False)
    channel         = Column('channel', String(255), nullable=True)
    created_at      = Column('created_at', TIMESTAMP(), nullable=False, default=datetime.utcnow)
    status          = Column('status', Integer(),     nullable=False, default=1)


    @classmethod
    def list_not_acked(cls, sender_id:Text, session=None, *args, **kwargs) -> List[Dict]:
        if session is None:
            session = get_session()

        messages = session.query(cls).filter_by(status=1, sender_id=sender_id, type='bot_uttered').all()

        return [{'id': m.id, 'body': m.body, 'created_at': m.created_at.strftime("%Y-%m-%dT%H:%M:%S")} for  m in messages]
        

    @classmethod
    def ack(cls, *args, **kwargs):
        
        id:Union[List[int], int] = args[0] if len(args) == 1 else None
        session =  args[1] if len(args) == 2 else None 
        
        if id is None:
            logger.error("Sem ack!")
            return

        if session is None:
            session = get_session()
       
        if not isinstance(id, List): 
            id = [id]
        
        session.query(cls).filter(cls.id.in_(id)).update({'status': 2}, synchronize_session=False)

        session.commit()


    @property
    def body(self) -> dict:
        from json import loads
        return loads(self._body)


    @body.setter
    def body(self, value):
        from json import dumps
        self._body = dumps(value)   
        

class ModelStatus(enum.Enum):  
    ERROR_PROCESSING    = -1   
    PROCESSING          = 0
    CIA_NOT_FOUND       = 1
    COMMON_NOT_FOUND    = 2
    CIA_FOUND           = 3
    CONCLUDED           = 4
    CONCLUDED_WITH_EXCEPTION = 5


def create_database(
    path:Text=None, 
    *args, 
    **kwargs):
    engine = get_engine(path, *args, **kwargs)
 
    session = sessionmaker()
    
    session.configure(bind=engine)

    Base.metadata.create_all(engine)


def get_engine(
    path:Text=None, 
    *args, 
    **kwargs):
    global Engine

    if not path:
        #raise RuntimeError(f'Please, verify the path to db: {path}')
        path = DomainConfig.DB

    logger.debug(f'Path to db: {path}')

    if not Engine:
        Engine = create_engine(path, echo=False, **kwargs)
    
    return Engine


def get_session(path:Text=None, *args, **kwargs):
    session_factory = sessionmaker(bind=get_engine(path))
    session = scoped_session(session_factory)

    return session()


def is_valid(session, *args, **kwargs):
    try:
        session.connection()
    except Exception:
        return False

    return True


def close_session(session):
    session.commit()
    session.close()
    