import requests
from chatbot_utils.models import Solicitation
from chatbot_utils.config import BaseConfig as config

from typing import Text, Union, List


class Output(object):
    
    def __init__(self, solicitation:Solicitation, *args, **kwargs):
        self._solicitation = solicitation


    @classmethod
    def choose_output(self, type:Text, *args, **kwargs):
        _types = {'telegram': TelegramOutput, 'socketio': SocketioOutput}
        
        if type not in _types: 
            raise RuntimeError("Verifique se o tipo ({}) de saída está definido: {}".format(type, _types.keys()))

        return _types[type] 
    

    def send(self, message:Union[Text, List[Text]], *args, **kwargs) -> bool:
        raise NotImplementedError()


class TelegramOutput(Output):
    def __init__(self, solicitation:Solicitation, token:Text=None, *args, **kwargs):
        super().__init__(solicitation, *args, **kwargs)
        self._token = token if token else config.TELEGRAM_TOKEN


    @property
    def url(self):
        return 'https://api.telegram.org/bot'+ self._token +'/sendMessage' 
 
    
    def send(self, message:Union[Text, List[Text]], *args, **kwargs) -> bool:
        if isinstance(message, Text):
            message = [message]

        for m in message:
            payload = {'chat_id' : self._solicitation.sender_id, 'text': m}
            payload = {**payload, **kwargs}
            requests.post(self.url, json=payload)

        return True


class SocketioOutput(Output):
    def __init__(self, solicitation:Solicitation, token:Text=None, *args, **kwargs):
        super().__init__(solicitation, *args, **kwargs)


    @property
    def url(self):
        from chatbot_utils.config import BaseConfig as config

        url = config.RASA_SERVER_URL
        port = config.RASA_SERVER_PORT 
        
        return f'http://{url}:{port}/webhooks/socketio/append'
 
    
    def send(self, message:Union[Text, List[Text]], *args, **kwargs) -> bool:
        if isinstance(message, Text):
            message = [message]

        for m in message:
            payload = {'sender_id' : self._solicitation.sender_id, 'text': m}
            payload = {**payload, **kwargs}
            requests.post(self.url, json=payload)

        return True


