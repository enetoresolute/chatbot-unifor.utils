



class FlowException(Exception):
    def __init__(self, solicitation_id, description="Erro durante a execução do fluxo.", title="Erro no Fluxo", code=1, *args, **kwargs):
        super().__init__(description)
        self.solicitation_id = solicitation_id
        self.title = title
        self.code = code


class CiaNotFoundException(Exception):
    def __init__(self, solicitation_id, description, title="Nenhum Cia Encontrado", code=2, *args, **kwargs):
        super().__init__(description)
        self.solicitation_id = solicitation_id
        self.title = title
        self.code = code


class StopSolicitationException(Exception):
    def __init__(self, solicitation_id, description="Solicitação encerrada!.", title="Erro de Solicitação", code=3, *args, **kwargs):
        super().__init__(description)
        self.solicitation_id = solicitation_id
        self.title = title
        self.code = code


class AlreadyExistsFirstPaymentException(Exception):
    def __init__(self, solicitation_id, description, title="Alerta Presente durante Solicitação", code=4, *args, **kwargs):
        super().__init__(description)
        self.solicitation_id = solicitation_id
        self.description = description
        self.title = title
        self.code = code


class ErrorWhileGetPdfException(Exception):
    def __init__(self, solicitation_id, description, title="Alerta Presente durante Solicitação", code=4, *args, **kwargs):
        super().__init__(description)
        self.solicitation_id = solicitation_id
        self.description = description
        self.title = title
        self.code = code
