import logging
from datetime import datetime, date
from typing import Text, List


def get_logger(
    level=logging.DEBUG, 
    format="%(levelname)s (%(filename)s:%(lineno)s): (%(asctime)s) %(message)s",
    datefmt="%d-%m %H:%M:%S",
    *args, **kwargs):
    
    logging.basicConfig(level=logging.DEBUG, format="%(levelname)s (%(filename)s:%(lineno)s): (%(asctime)s) %(message)s", datefmt="%d-%m %H:%M:%S")
    
    return logging.getLogger(__name__)


def parse_datetime(
    datetime_str, 
    formats=['%d/%m/%Y %H:%M', '%Y-%m-%dT%H:%M:%SZ', '%Y-%m-%dT%H:%MZ', '%Y-%m-%dT%H:%M:%S.%fZ', '%Y-%m-%dT%H:%M:%S.%f'], 
    exception=RuntimeError,
    exception_message="Verifique a formatação da data. Ela deve está no formato: ",
    *args,
    **kwargs):
    import dateutil.parser

    logger.debug("Parseando: {}".format(datetime_str))

    for __format in formats:
        try:
            return datetime.strptime(datetime_str, __format)         
        except ValueError as err:
            logger.debug("Erro ao tentar fazer o parse da data: {}".format(str(err)))
            
            try:
                return dateutil.parser.parse(datetime_str).replace(tzinfo=None)
            except Exception as err:
                logger.error("Erro na tentativa de parsear para isoformat: {}".format(err))
            
            #if kwargs.get('make_recursion', True):
            #    return parse_datetime(datetime_str.split('+')[0], make_recursion=False) 

    exception_message = exception_message + ", ".join(formats) + "."
    raise exception(exception_message)


def parse_date( 
    datetime_str: Text, 
    formats=['%d/%m/%Y', '%Y-%m-%d'],
    exception=RuntimeError,
    exception_message="Verifique a formatação da data. Ela deve está no formato: ",
    *args,
    **kwargs) -> datetime:

    if 'T' in datetime_str: datetime_str = datetime_str.split('T')[0]
    
    for __format in formats:
        try:
            return datetime.strptime(datetime_str, __format)         
        except ValueError as err:
            logger.debug("Erro ao tentar fazer o parse da data: {}".format(str(err)))

    exception_message = exception_message + ", ".join(formats) + "."
    
    raise exception(exception_message)


def mount_url(path:Text='credentials.yml', *args, **kwargs) -> Text:
    import yaml

    url_path: Text = None

    with open(path, 'r') as stream:
        try:
            yml = yaml.safe_load(stream)

            if 'telegram' in yml and 'webhook_url' in yml['telegram']:
                url_path = yml['telegram']['webhook_url'].split('/webhooks')[0]

        except yaml.YAMLError as exc:
            logger.exception(exc)

        except Exception as err:
            logger.exception(err)

    return url_path


def holydays(year:int=None, *args, **kwargs) -> List[Text]:
    from workalendar.america import BrazilFortalezaCity

    return [str(tup[0]) for tup in BrazilFortalezaCity().holidays(datetime.now().year)]


def get_redis_conn(
    redis_url:Text=None,
    redis_password:Text=None,
    sentinel_master_node:Text=None,
    sentinel_port:Text=None,
    *args, **kwargs):
    """
    Função para retornar uma conexão válida com o Redis ou Sentinel, com base nas variáveis passadas via linha de comando.

    Quando as variaveis do Sentinel estiverem definidas, automaticamente esse tipo de conexão será feita. Caso contrário, uma conexão simples com
    o Redis será retornada.
    """
    from redis import Redis
    from redis.sentinel import Sentinel
    
    logger.info(f'Redis/Sentinel conf:\n- Redis Url: {redis_url}\n- Redis Password: {redis_password}\n- Sentinel Master Node: {sentinel_master_node}\n- Sentinel Port: {sentinel_port}')
    
    if sentinel_master_node and sentinel_port:
        logger.info(f'Init with Sentinel...')
        sentinel = Sentinel([(redis_url, sentinel_port)])
        return sentinel.master_for(sentinel_master_node)
    
    logger.info(f'Init with Redis...')
    return Redis(host=redis_url, password=redis_password)


def get_class_with_path(
    path:Text,
    *args,
    **kwrgs):
    """
    Função para pegar uma classe com base no `path` passado como argumento.
    """
    from inspect import isclass

    components = path.split('.')

    logger.debug(f'Components: {components}')
    
    mod = __import__(components[0])

    for comp in components[1:]:
        mod = getattr(mod, comp)

    if not isclass(mod):
        return None

    return mod

logger = get_logger()