# Projeto Chatbot Unifor - Pacote Utils

Pacote com modelos de dados, implementações dos *RPAs*, *command line interface* e funções para o projeto.

Esse projeto se faz presente em todos os outros (Core, Actions e RPA) através das dependências nos **requirements**: 

- -e git+https://enetoresolute@bitbucket.org/enetoresolute/chatbot-unifor.utils.git#egg=Chatbot_Unifor_Utilidades
- -e git+https://gitlab.unifor.br/chatbot/chatbot.utils.git#egg=Chatbot_Unifor_Utilidades
