from setuptools import setup

setup(
    name='Chatbot Unifor - Utilidades',
    url='https://enetoresolute@bitbucket.org/enetoresolute/chatbot-unifor.utils.git',
    author='Eduardo Neto',
    author_email='eneto@resoluteit.com.br',
    packages=['chatbot_utils', 'chatbot_utils.models', 'chatbot_utils.exceptions', 'chatbot_utils.rpa'],
    install_requires=[
        'SQLAlchemy==1.3.19', 
        'redis==3.5.3', 
        'workalendar==12.0.0', 
        'selenium==3.141.0', 
        'rq==1.5.2', 
        'requests==2.24.0',
        'fake-useragent==0.1.11',
        'psycopg2-binary==2.8.6',
        'cx-Oracle==8.0.1',
        'click==7.1.2'
        ],
    version='0.3',
    license='MIT',
)